# Libraies used-------------------------------------------------------------------
from onewire import *
import ads1115
from machine import Pin, I2C, UART
from time import sleep
from machine import UART
import os
#Define pins--------------------------------------------------------------------
E_5v = Pin('P2', Pin.OUT) # 5v enable line
IVY = Pin('P3', Pin.OUT) # IVY enable line

# The following pins controll the MUX pins
s0 = Pin('P7', Pin.OUT, pull=Pin.PULL_UP)
s1 = Pin('P6', Pin.OUT, pull=Pin.PULL_UP)
s2 = Pin('P5', Pin.OUT, pull=Pin.PULL_UP)
s3 = Pin('P4', Pin.OUT, pull=Pin.PULL_UP)
ON_LED = Pin('P12', Pin.OUT, pull=Pin.PULL_UP)
Dallas = DS2431(OneWire(Pin('P21'))) #OneWire pin

#Digital pins
P0 = Pin('P23', Pin.IN, pull=Pin.PULL_UP)
P1 = Pin('P22', Pin.IN, pull=Pin.PULL_UP)
P2 = Pin('P8', Pin.IN, pull=Pin.PULL_UP)
P3 = Pin('P11', Pin.IN, pull=Pin.PULL_UP)
P4 = Pin('P20', Pin.IN, pull=Pin.PULL_UP)

#Define variables---------------------------------------------------------------
Results = ["    ","",""],["    ","",""],["    ","",""],["    ","",""],["    ","",""] #results are stored here
DevMode=False # Enables sub fuctions to display their data

#set pins-----------------------------------------------------------------------
IVY.value(1) #Turn off the IVY line
E_5v.value(1) #Turn off the 5v line
ON_LED.value(1)
# ADS1115 i2c pins
i2c = I2C(0, pins=('P9','P10'))
i2c.init(mode=I2C.MASTER, baudrate=20000)
adc = ads1115.ADS1115(i2c,0x48, gain=0)

#List of sensors----------------------------------------------------------------

# ["Name" , ID, b'data'],
SensorList=[
    ["Water Level 0-6m 4-20mA" , 100, b'\x08\x01\x00d\x00\x00\x02XP\xc4\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Rain Gauge",109,b'\x08\x01\x00m\x00\x00\x00\x00\xec\xdd\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Water level 0-6m 5v" , 110, b'\x08\x01\x00n\x00\x00\x00\x00\xf1\x11\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Ultra Sonic Horn",111,b'\x08\x01\x00o\x00\x00\x01\xf4R&\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Pressure Sensor 4-20mA",117,b'\x08\x01\x00u\x00\x00\t\xc4U\x8d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Pressure Sensor 5v",118,b'\x08\x01\x00v\x00\x00\t\xc4HA\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Pneumatic Water level 5v" , 124, b'\x08\x01\x00|\x00\x00\x01\xf4\xfb\xaa\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Diesel",125,b'\x08\x01\x00}\x00\x00\x01\x90\xd5\xcc\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Water Level via Pressure" , 126, b'\x08\x01\x00~\x00\x00\x01\x04\x1a\xad\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["50m Bore",127,b'\x08\x01\x00\x7f\x00\x00\x13\x88\xf9\xac\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Flow Meter",128,b'\x08\x01\x00\x80\x00\x00\x00\x00\xbbO\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Absolute Hydrostatic Water Level",129,b'\x08\x01\x00\x81\x00\x00\x02X]v\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Dam 0-8m",138,b'\x08\x01\x00\x8a\x00\x00\x03 \xfc\x8d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["80m Bore",141,b'\x08\x01\x00\x8d\x00\x00\x1f@\x93f\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["UAN 0-6m 5v" , 142, b'\x08\x01\x00\x8e\x00\x00\x01\xcd\xde\xc6\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Machine Control Off" , 852, b'\x08\x01\x03T\x00\x00\x00\x00e\x15\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Machine Control On" , 853, b'\x08\x01\x03U\x00\x00\x00\x00nQ\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["SOS Box Visit button" , 855, b'\x08\x01\x03W\x00\x00\x00\x00x\xd9\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["SOS Box SOS button" , 856, b'\x08\x01\x03X\x00\x00\x00\x00\x12%\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Camera" , 5000, b'\x08\x01\x13\x88\x00\x01\x00\x0c?\x12\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'],
    ["Unprogrammed",0,b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00']
    ]

#Sub Functions------------------------------------------------------------------
def SetPort(x): #sets the port from the input variable
    x=str(bin(x)[2:])#converts number to binary
    v="000"+str(x) #converts number to 4 digits
    v=v[-4:]
    s0.value(int(v[3])) # sets the corrospondings pins high/low
    s1.value(int(v[2]))
    s2.value(int(v[1]))
    s3.value(int(v[0]))

def ENABLE_IVY():
    if DevMode==True:
        print("\n"+"IVY enabled")
    E_5v.value(1)# turns off 5v line
    IVY.value(0)# turns on IVY line

def DISABLE_IVY():
    if DevMode==True:
        print("\n"+"IVY disabled")
    IVY.value(1) #turns off IVY line

def ENABLE_5V():
    if DevMode==True:
        print("\n"+"5v enabled")
    IVY.value(1)#turns off IVY line
    E_5v.value(0)# turns on 5v line

def DISABLE_5V():
    if DevMode==True:
        print("\n"+"5v disabled")
    E_5v.value(1)# turns off 5v line

def ReadDallas(): #Reads dallas chip, if value matches something in the list it displays it
    if DevMode==True:
        print("\n"+"Begin ReadDallas")
    for i in range(0,5):
        SetPort(i)
        Dallas = DS2431(OneWire(Pin('P21')))
        sleep(0.1)
        if Dallas.avaliable():
            ID=Dallas.read_memory(24)
            for B in range(0,len(SensorList)):
                if SensorList[B][2]==ID:
                    print("Port "+str(i)+": "+str(SensorList[B][0])+" ("+str(SensorList[B][1])+")")
                    break
                else:
                    if B == len(SensorList)-1:
                        print("Port "+str(i)+": Unknown Dallas Code")
        else:
            print("Port "+str(i)+": No Dallas")

def MeasureWaterLevel(TYPE): #cycles trough all the ports and takes their voltages
    if DevMode==True:
        print("\n"+"Begin MeasureWaterLevel")
    for i in range(0,5):
        SetPort(i)
        sleep(1)
        voltage=adc.raw_to_v(adc.read(rate=6, channel1=0))

        if TYPE == "Pressure4-20mA":
            level=((((voltage)-0.5)/4)*2500)
        if TYPE == "Diesel":
            level=((((voltage)-0.5)/4)*494)
        if TYPE == "Bore50m":
            level=((((voltage)-0.5)/4)*50)
        if TYPE == "ABS5v":
            level=((((voltage)-0.5)/4)*600)
        if TYPE == "Bore80m":
            level=((((voltage)-0.5)/4)*80)
        if TYPE == "UAN":
            level=((((voltage)-0.5)/4)*600)

        Results[i][2]=round(level,1)
        if DevMode==True:
            print("Port No: "+str(i))
            print("Voltage= "+str(voltage))
            print("Level= "+str(level))

def MeasureWaterLevelDam(Depth): #Dam is diffrent as the max depth changes
    if DevMode==True:
        print("\n"+"Begin MeasureWaterLevelDam")
    for i in range(0,5):
        SetPort(i)
        sleep(1)
        voltage=adc.raw_to_v(adc.read(rate=6, channel1=0))
        level=(((voltage-0.5)/4)*Depth)
        Results[i][2]=round(level)
        if DevMode==True:
            print("Port No: "+str(i))
            print("Voltage= "+str(voltage))
            print("Level= "+str(level))

def WriteDallas(ID): #Search for the ID in SensorList then write its data to the dallas chip
    if DevMode==True:
        print("\n"+"Begin WriteDallas")

    #Find the binary in the list by searching for the ID
    for i in range(0,len(SensorList)):
        if SensorList[i][1]==int(ID):
            DATA=SensorList[i][2]
            break
        if i == len(SensorList)-1:
            print("ERROR INVALID ID ENTERED")
            print(StopProgram) #Stop the program from running if id is missing

    # Write the binary to all ports with dallas chips
    for i in range(0,5):
        SetPort(i)
        Dallas = DS2431(OneWire(Pin('P21')))
        sleep(0.3)
        if Dallas.avaliable():
            Dallas.write_memory(bytearray(DATA))
            if DevMode==True:
                print("Port "+str(i)+": programed as "+str(ID))
        else:
            if DevMode==True:
                print("Port "+str(i)+": No Dallas")
            Results[i][1]="NONE"

def ConfirmDallas(ID): #Reads dallas chip, if value matches something in the list it displays it
    if DevMode==True:
        print("\n"+"Begin ConfirmDallas")
    for i in range(0,len(SensorList)):
        if SensorList[i][1]==int(ID):
            DATA=SensorList[i][2]
    for i in range(0,5):
        SetPort(i)
        Dallas = DS2431(OneWire(Pin('P21')))
        sleep(0.1)
        if Dallas.avaliable():
            BIN=Dallas.read_memory(24)
            if BIN==DATA:
                Results[i][1]="GOOD"
                if DevMode==True:
                    print("Dallas confirmed")
            else:
                Results[i][1]="FAILED TO PROGRAM DALLAS"
                if DevMode==True:
                    print("Dallas Failed")
        else:
            Results[i][1]="NONE"



def DispDataProbe(value):
    if DevMode==True:
        print("\n"+"Begin DispDataProbe")
    print(" PORT NUM | STATUS | DALLAS | Measured value / Inputted value")
    for i in range(0,5):
        print("  Port "+str(i)+"  |  "+str(Results[i][0])+"  |  "+str(Results[i][1])+"  |  "+str(Results[i][2])+" / " + str(value)      )

def DispDataDigital():
    if DevMode==True:
        print("\n"+"Begin DispDataDigital")
    print(" PORT NUM | DALLAS ")
    for i in range(0,5):
        print("  Port "+str(i)+"  |  "+str(Results[i][1]))

def StatusChk(value, Error):
    if DevMode==True:
        print("\n"+"Begin StatusChk")
    for i in range(0,5):
        #if int(Results[i][2])<=(value+Error) and int(Results[i][2])>=(value-Error):
        if float(Results[i][2])<=float(value+Error) and float(Results[i][2])>=float(value-Error):
            if str(Results[i][1])=="GOOD":
                Results[i][0]="PASS"
            else:
                Results[i][0]="FAIL"
        else:
            Results[i][0]="FAIL"

def DigitalTest():
    if DevMode==True:
        print("\n"+"Begin DigitalTest")
    Port0=0
    Port1=0
    Port2=0
    Port3=0
    Port4=0
    Prt0ENB=True
    Prt1ENB=True
    Prt2ENB=True
    Prt3ENB=True
    Prt4ENB=True
    sleep(0.2)
    ser = UART(0, baudrate=115200)
    while True:
        if ser.any()>0:
            break

        if P0()==0 and Prt0ENB==True:
            Port0+=1
            Prt0ENB=False
        if P0()==1 and Prt0ENB==False:
            Prt0ENB=True
        if P1()==0 and Prt1ENB==True:
            Port1+=1
            Prt1ENB=False
        if P1()==1 and Prt1ENB==False:
            Prt1ENB=True
        if P2()==0 and Prt2ENB==True:
            Port2+=1
            Prt2ENB=False
        if P2()==1 and Prt2ENB==False:
            Prt2ENB=True
        if P3()==0 and Prt3ENB==True:
            Port3+=1
            Prt3ENB=False
        if P3()==1 and Prt3ENB==False:
            Prt3ENB=True
        if P4()==0 and Prt4ENB==True:
            Port4+=1
            Prt4ENB=False
        if P4()==1 and Prt4ENB==False:
            Prt4ENB=True

        print("Port 4: "+str(Port4)+" | Port 3: "+str(Port3)+" | Port 2: "+str(Port2)+" | Port 1: "+str(Port1)+" | Port 0: "+str(Port0),end='\r')
    os.dupterm(ser)


def Name_Of_Test(type):
    if type == "Rain Gauge":
        print("\n\n")
        print("                    .______           ___       __  .__   __.      _______      ___       __    __    _______  _______")
        print("                    |   _  \         /   \     |  | |  \ |  |     /  _____|    /   \     |  |  |  |  /  _____||   ____|")
        print("                    |  |_)  |       /  ^  \    |  | |   \|  |    |  |  __     /  ^  \    |  |  |  | |  |  __  |  |__")
        print("                    |      /       /  /_\  \   |  | |  . `  |    |  | |_ |   /  /_\  \   |  |  |  | |  | |_ | |   __|")
        print("                    |  |\  \----. /  _____  \  |  | |  |\   |    |  |__| |  /  _____  \  |  `--'  | |  |__| | |  |____ ")
        print("                    | _| `._____|/__/     \__\ |__| |__| \__|     \______| /__/     \__\  \______/   \______| |_______|")
        print("\n\n")
    if type == "Flow Meter":
        print("\n\n")
        print("                     _______  __        ______   ____    __    ____    .___  ___.  _______ .___________. _______ .______")
        print("                    |   ____||  |      /  __  \  \   \  /  \  /   /    |   \/   | |   ____||           ||   ____||   _  \ ")
        print("                    |  |__   |  |     |  |  |  |  \   \/    \/   /     |  \  /  | |  |__   `---|  |----`|  |__   |  |_)  |")
        print("                    |   __|  |  |     |  |  |  |   \            /      |  |\/|  | |   __|      |  |     |   __|  |      /")
        print("                    |  |     |  `----.|  `--'  |    \    /\    /       |  |  |  | |  |____     |  |     |  |____ |  |\  \----.")
        print("                    |__|     |_______| \______/      \__/  \__/        |__|  |__| |_______|    |__|     |_______|| _| `._____|")
        print("\n\n")
    if type == "5v Probe":
        print("\n\n")
        print("                     _____  ____    ____    .______   .______        ______   .______    _______ ")
        print("                    | ____| \   \  /   /    |   _  \  |   _  \      /  __  \  |   _  \  |   ____|")
        print("                    | |__    \   \/   /     |  |_)  | |  |_)  |    |  |  |  | |  |_)  | |  |__ ")
        print("                    |___ \    \      /      |   ___/  |      /     |  |  |  | |   _  <  |   __|")
        print("                     ___) |    \    /       |  |      |  |\  \----.|  `--'  | |  |_)  | |  |____ ")
        print("                    |____/      \__/        | _|      | _| `._____| \______/  |______/  |_______|")
        print("\n\n")
    if type == "Pressure 420":
        print("\n\n")
        print("                    .______   .______       _______      _______.     _______. __    __  .______       _______     _  _             ___     ___")
        print("                    |   _  \  |   _  \     |   ____|    /       |    /       ||  |  |  | |   _  \     |   ____|   | || |           |__ \   / _ \ ")
        print("                    |  |_)  | |  |_)  |    |  |__      |   (----`   |   (----`|  |  |  | |  |_)  |    |  |__      | || |_   ______    ) | | | | |")
        print("                    |   ___/  |      /     |   __|      \   \        \   \    |  |  |  | |      /     |   __|     |__   _| |______|  / /  | | | |")
        print("                    |  |      |  |\  \----.|  |____ .----)   |   .----)   |   |  `--'  | |  |\  \----.|  |____       | |            / /_  | |_| |")
        print("                    | _|      | _| `._____||_______||_______/    |_______/     \______/  | _| `._____||_______|      |_|           |____|  \___/ ")
        print("\n\n")
    if type == "Disel":
        print("\n\n")
        print("                     _______   __   _______      _______. _______  __")
        print("                    |       \ |  | |   ____|    /       ||   ____||  |")
        print("                    |  .--.  ||  | |  |__      |   (----`|  |__   |  |")
        print("                    |  |  |  ||  | |   __|      \   \    |   __|  |  |")
        print("                    |  '--'  ||  | |  |____ .----)   |   |  |____ |  `----.")
        print("                    |_______/ |__| |_______||_______/    |_______||_______|")
        print("\n\n")
    if type == "Bore50m":
        print("\n\n")
        print("                    .______     ______   .______       _______     _____    ___   .___  ___.")
        print("                    |   _  \   /  __  \  |   _  \     |   ____|   | ____|  / _ \  |   \/   |")
        print("                    |  |_)  | |  |  |  | |  |_)  |    |  |__      | |__   | | | | |  \  /  |")
        print("                    |   _  <  |  |  |  | |      /     |   __|     |___ \  | | | | |  |\/|  |")
        print("                    |  |_)  | |  `--'  | |  |\  \----.|  |____     ___) | | |_| | |  |  |  |")
        print("                    |______/   \______/  | _| `._____||_______|   |____/   \___/  |__|  |__|")
        print("\n\n")
    if type == "dam":
        print("\n\n")
        print("                     _______       ___      .___  ___.")
        print("                    |       \     /   \     |   \/   |")
        print("                    |  .--.  |   /  ^  \    |  \  /  |")
        print("                    |  |  |  |  /  /_\  \   |  |\/|  |")
        print("                    |  '--'  | /  _____  \  |  |  |  |")
        print("                    |_______/ /__/     \__\ |__|  |__|")
        print("\n\n")
    if type == "Bore 80m":
        print("\n\n")
        print("                    .______     ______   .______       _______      ___     ___   .___  ___.")
        print("                    |   _  \   /  __  \  |   _  \     |   ____|    / _ \   / _ \  |   \/   |")
        print("                    |  |_)  | |  |  |  | |  |_)  |    |  |__      | (_) | | | | | |  \  /  |")
        print("                    |   _  <  |  |  |  | |      /     |   __|      > _ <  | | | | |  |\/|  |")
        print("                    |  |_)  | |  `--'  | |  |\  \----.|  |____    | (_) | | |_| | |  |  |  |")
        print("                    |______/   \______/  | _| `._____||_______|    \___/   \___/  |__|  |__|")
        print("\n\n")
    if type == "UAN":
        print("\n\n")
        print("                     __    __       ___      .__   __.")
        print("                    |  |  |  |     /   \     |  \ |  |")
        print("                    |  |  |  |    /  ^  \    |   \|  |")
        print("                    |  |  |  |   /  /_\  \   |  . `  |")
        print("                    |  `--'  |  /  _____  \  |  |\   |")
        print("                     \______/  /__/     \__\ |__| \__|")
        print("\n\n")
    if type == "Camera":
        print("\n\n")
        print("                      ______      ___      .___  ___.  _______ .______           ___      ")
        print("                     /      |    /   \     |   \/   | |   ____||   _  \         /   \     ")
        print("                    |  ,----'   /  ^  \    |  \  /  | |  |__   |  |_)  |       /  ^  \    ")
        print("                    |  |       /  /_\  \   |  |\/|  | |   __|  |      /       /  /_\  \   ")
        print("                    |  `----. /  _____  \  |  |  |  | |  |____ |  |\  \----. /  _____  \  ")
        print("                     \______|/__/     \__\ |__|  |__| |_______|| _| `._____|/__/     \__\ ")
        print("\n\n")

def List():
    print("\n")
    print("Version 1.3")
    print("List of functions:")
    print("EnableDevMode()")
    print("RainGauge()")
    print("Pressure420()")
    print("Diesel()")
    print("Bore50m()")
    print("FlowMeter()")
    print("WaterLevelAbsolute5v()")
    print("Dam()")
    print("Bore80m()")
    print("UAN()")
    print("ReadDallas()")
    print("Camera()")

#FUNCTUONS--------------------------------------------------------------------------------------------------------------
def EnableDevMode():
    global DevMode
    DevMode=True
    print("\n")
    print("Welcome to DevMode! Sub Functions:")
    print("SetPort(x)")
    print("ENABLE_IVY()")
    print("DISABLE_IVY()")
    print("ENABLE_5V()")
    print("DISABLE_5V()")
    print("ReadDallas()")
    print("MeasureWaterLevel()")
    print("WriteDallas()")
    print("DispData()")
    print("StatusChk(value)")
    print("List()")


def RainGauge(): #make new Data display for rain gauge and flow meters
    Name_Of_Test("Rain Gauge")
    WriteDallas(109)
    ConfirmDallas(109)
    DispDataDigital()
    print("Press any key to exit this mode")
    DigitalTest()
    List()

def Pressure420():
    Name_Of_Test("Pressure 420")
    value=float(input('Type in the test pressure in KPA: '))
    WriteDallas(117)
    ConfirmDallas(117)
    ENABLE_IVY()
    MeasureWaterLevel("Pressure4-20mA")
    DISABLE_IVY()
    StatusChk(value, 13)
    DispDataProbe(value)
    List()

def Diesel():
    Name_Of_Test("Disel")
    value=float(input('Type in the test depth in cm: '))
    WriteDallas(125)
    ConfirmDallas(125)
    ENABLE_IVY()
    MeasureWaterLevel("Diesel")
    DISABLE_IVY()
    StatusChk(value, 2)
    DispDataProbe(value)
    List()

def Bore50m():
    Name_Of_Test("Bore50m")
    value=int(input('Type in the test depth in m: '))
    WriteDallas(127)
    ConfirmDallas(127)
    ENABLE_IVY()
    MeasureWaterLevel("Bore50m")
    DISABLE_IVY()
    StatusChk(value, 2.5)
    DispDataProbe(value)
    List()

def FlowMeter():
    Name_Of_Test("Flow Meter")
    WriteDallas(128)
    ConfirmDallas(128)
    DispDataDigital()
    print("Press any key to exit this mode")
    DigitalTest()
    List()

def WaterLevelAbsolute5v():
    Name_Of_Test("5v Probe")
    value=float(input('Type in the test depth in cm: '))
    WriteDallas(129)
    ConfirmDallas(129)
    ENABLE_5V()
    MeasureWaterLevel("ABS5v")
    DISABLE_5V()
    StatusChk(value, 3)
    DispDataProbe(value)
    List()

def UAN():
    Name_Of_Test("UAN")
    value=float(input('Type in the test depth in cm: '))
    WriteDallas(142)
    ConfirmDallas(142)
    ENABLE_5V()
    MeasureWaterLevel("UAN")
    DISABLE_5V()
    StatusChk(value, 3)
    DispDataProbe(value)
    List()

def Dam():
    Name_Of_Test("dam")
    Depth=float(input('Type in the max probe depth cm: '))
    value=float(input('Type in the test depth in cm: '))
    WriteDallas(138)
    ConfirmDallas(138)
    ENABLE_IVY()
    MeasureWaterLevelDam(Depth)
    DISABLE_IVY()
    StatusChk(value, Depth*0.01)
    DispDataProbe(value)
    List()

def Bore80m():
    Name_Of_Test("Bore 80m")
    value=float(input('Type in the test depth in m: '))
    WriteDallas(141)
    ConfirmDallas(141)
    ENABLE_IVY()
    MeasureWaterLevel("Bore80m")
    DISABLE_IVY()
    StatusChk(value, 4)
    DispDataProbe(value)
    List()

def Camera():
    Name_Of_Test("Camera")
    WriteDallas(5000)
    ConfirmDallas(5000)
    DispDataDigital()
    List()
